<!-- small box Tổng doanh thu-->
                <div class="col-lg-4 col-6" style="padding: 0 14px">
                  <div
                    class="small-box"
                    style="
                      background: #fbf2ef;
                      filter: drop-shadow(
                        22px 22px 36px rgba(44, 44, 44, 0.08)
                      );
                    "
                  >
                    <div class="inner" style="padding: 27px 21px">
                      <!-- title -->
                      <div
                        class="d-flex align-items-center"
                        style="margin-bottom: 22px"
                      >
                        <img
                          src="dist/img/home-page/revenue.png"
                          alt=""
                          style="width: 56px"
                        />
                        <h4
                          class="font-weight-bold"
                          style="margin-left: 12px; margin-bottom: 0"
                        >
                          TỔNG DOANH THU (VNĐ)
                        </h4>
                      </div>
                      <!-- số liệu -->
                      <div class="soLieu">
                        <h3>500.000.000</h3>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- small box Tổng đơn hàng-->
                <div class="col-lg-4 col-6" style="padding: 0 14px">
                  <div
                    class="small-box"
                    style="
                      background: #ecf2ff;
                      filter: drop-shadow(
                        22px 22px 36px rgba(44, 44, 44, 0.08)
                      );
                    "
                  >
                    <div class="inner" style="padding: 27px 21px">
                      <!-- title -->
                      <div
                        class="d-flex align-items-center"
                        style="margin-bottom: 22px"
                      >
                        <img
                          src="dist/img/home-page/customerpng.png"
                          alt=""
                          style="width: 56px"
                        />
                        <h4
                          class="font-weight-bold"
                          style="margin-left: 12px; margin-bottom: 0"
                        >
                          TỔNG ĐƠN HÀNG
                        </h4>
                      </div>
                      <!-- số liệu -->
                      <div class="soLieu">
                        <h3>199</h3>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- small box Tổng sản phẩm bán -->
                <div class="col-lg-4 col-6" style="padding: 0 14px">
                  <div
                    class="small-box"
                    style="
                      background: #e6fffa;
                      filter: drop-shadow(
                        22px 22px 36px rgba(44, 44, 44, 0.08)
                      );
                    "
                  >
                    <div class="inner" style="padding: 27px 21px">
                      <!-- title -->
                      <div
                        class="d-flex align-items-center"
                        style="margin-bottom: 22px"
                      >
                        <img
                          src="dist/img/home-page/product.png"
                          alt=""
                          style="width: 56px"
                        />
                        <h4
                          class="font-weight-bold"
                          style="margin-left: 12px; margin-bottom: 0"
                        >
                          TỔNG SẢN PHẨM BÁN
                        </h4>
                      </div>
                      <!-- số liệu -->
                      <div class="soLieu">
                        <h3>307</h3>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- small box Tổng đơn đặt hàng-->
                <div class="col-lg-4 col-6" style="padding: 0 14px">
                  <div
                    class="small-box"
                    style="
                      background: #fbf2ef;
                      filter: drop-shadow(
                        22px 22px 36px rgba(44, 44, 44, 0.08)
                      );
                    "
                  >
                    <div class="inner" style="padding: 27px 21px">
                      <!-- title -->
                      <div
                        class="d-flex align-items-center"
                        style="margin-bottom: 22px"
                      >
                        <img
                          src="dist/img/home-page/revenue.png"
                          alt=""
                          style="width: 56px"
                        />
                        <h4
                          class="font-weight-bold"
                          style="margin-left: 12px; margin-bottom: 0"
                        >
                          ĐƠN ĐẶT HÀNG
                        </h4>
                      </div>
                      <!-- số liệu -->
                      <div class="soLieu">
                        <h3>25</h3>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- small box Tổng đơn đã xác nhận-->
                <div class="col-lg-4 col-6" style="padding: 0 14px">
                  <div
                    class="small-box"
                    style="
                      background: #ecf2ff;
                      filter: drop-shadow(
                        22px 22px 36px rgba(44, 44, 44, 0.08)
                      );
                    "
                  >
                    <div class="inner" style="padding: 27px 21px">
                      <!-- title -->
                      <div
                        class="d-flex align-items-center"
                        style="margin-bottom: 22px"
                      >
                        <img
                          src="dist/img/home-page/customerpng.png"
                          alt=""
                          style="width: 56px"
                        />
                        <h4
                          class="font-weight-bold"
                          style="margin-left: 12px; margin-bottom: 0"
                        >
                          ĐƠN HÀNG ĐÃ XÁC NHẬN
                        </h4>
                      </div>
                      <!-- số liệu -->
                      <div class="soLieu">
                        <h3>11</h3>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- small box Tổng đơn hàng đã giao -->
                <div class="col-lg-4 col-6" style="padding: 0 14px">
                  <div
                    class="small-box"
                    style="
                      background: #e6fffa;
                      filter: drop-shadow(
                        22px 22px 36px rgba(44, 44, 44, 0.08)
                      );
                    "
                  >
                    <div class="inner" style="padding: 27px 21px">
                      <!-- title -->
                      <div
                        class="d-flex align-items-center"
                        style="margin-bottom: 22px"
                      >
                        <img
                          src="dist/img/home-page/product.png"
                          alt=""
                          style="width: 56px"
                        />
                        <h4
                          class="font-weight-bold"
                          style="margin-left: 12px; margin-bottom: 0"
                        >
                          ĐƠN HÀNG ĐÃ GIAO
                        </h4>
                      </div>
                      <!-- số liệu -->
                      <div class="soLieu">
                        <h3>15</h3>
                      </div>
                    </div>
                  </div>
                </div>